﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto
{
    public class DropdownDto
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
