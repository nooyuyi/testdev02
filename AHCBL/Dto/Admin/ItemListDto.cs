﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AHCBL.Dto.Admin
{
    public class ItemListDto
    {
        public int id { get; set; }
        public string token { get; set; }
        public int product_id { get; set; }

        [MaxLength(20)]
        public string code { get; set; }

        [MaxLength(100)]
        public string name { get; set; }
        public string price_win { get; set; }
        public string price_interest { get; set; }

        [MaxLength(20)]
        public string start_date { get; set; }

        [MaxLength(20)]
        public string buy_name { get; set; }
        public string buy_date { get; set; }
        public string tran_date { get; set; }
        public string create_date { get; set; }
        public bool it_use { get; set; }
        public string win_date { get; set; }
        public string status { get; set; }

        public string img { get; set; }
        public int day { get; set; }
        public int percen { get; set; }
        public int chip { get; set; }
        public string price { get; set; }
        public int amount { get; set; }
        public int total { get; set; }
        public int point { get; set; }
        public SelectList category_list { get; set; }
    }
}
