﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class PointListDto
    {
        public int id { get; set; }
        public string username { get; set; }
        public string fullname { get; set; }

        [MaxLength(255)]
        public string name { get; set; }
        public string amount { get; set; }

        [MaxLength(20)]
        public string exp_date { get; set; }
        public string price { get; set; }
        public string create_date { get; set; }
        public int pay_type { get; set; }
        public string total { get; set; }
        public int member_id { get; set; }
        public string status { get; set; }
    }
}
