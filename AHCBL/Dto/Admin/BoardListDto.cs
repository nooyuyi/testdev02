﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AHCBL.Dto.Admin
{
    public class BoardListDto
    {
        public int id { get; set; }

        [MaxLength(20)]
        public string bo_table { get; set; }
        public int grp_id { get; set; }

        [MaxLength(120)]
        public string bo_subject { get; set; }

        [MaxLength(120)]
        public string bo_mobile_subject { get; set; }
        public int device_id { get; set; }
        public bool grp_device { get; set; }
        public bool all_device { get; set; }

        [MaxLength(255)]
        public string category_list { get; set; }
        public bool use_category { get; set; }
        public bool grp_category_list { get; set; }
        public bool all_category_list { get; set; }

        [MaxLength(20)]
        public string bo_admin { get; set; }
        public bool grp_admin { get; set; }
        public bool all_admin { get; set; }

        [MaxLength(2)]
        public string bo_list_level { get; set; }
        public bool grp_list_level { get; set; }
        public bool all_list_level { get; set; }

        [MaxLength(2)]
        public string bo_read_level { get; set; }
        public bool grp_read_level { get; set; }
        public bool all_read_level { get; set; }

        [MaxLength(2)]
        public string bo_write_level { get; set; }
        public bool grp_write_level { get; set; }
        public bool all_write_level { get; set; }

        [MaxLength(2)]
        public string reply_level { get; set; }
        public bool grp_reply_level { get; set; }
        public bool all_reply_level { get; set; }

        [MaxLength(2)]
        public string comment_level { get; set; }
        public bool grp_comment_level { get; set; }
        public bool all_comment_level { get; set; }

        [MaxLength(2)]
        public string link_level { get; set; }
        public bool grp_link_level { get; set; }
        public bool all_link_level { get; set; }

        [MaxLength(2)]
        public string upload_level { get; set; }
        public bool grp_upload_level { get; set; }
        public bool all_upload_level { get; set; }

        [MaxLength(2)]
        public string download_level { get; set; }
        public bool grp_download_level { get; set; }
        public bool all_download_level { get; set; }

        [MaxLength(2)]
        public string html_level { get; set; }
        public bool grp_html_level { get; set; }
        public bool all_html_level { get; set; }

        [MaxLength(3)]
        public string count_modify { get; set; }
        public bool grp_count_modify { get; set; }
        public bool all_count_modify { get; set; }

        [MaxLength(3)]
        public string count_delete { get; set; }
        public bool grp_count_delete { get; set; }
        public bool all_count_delete { get; set; }
        public bool use_sideview { get; set; }
        public bool grp_use_sideview { get; set; }
        public bool all_use_sideview { get; set; }
        public bool use_secret { get; set; }
        public bool grp_use_secret { get; set; }
        public bool all_use_secret { get; set; }
        public bool use_dhtml_editor { get; set; }
        public bool grp_use_dhtml_editor { get; set; }
        public bool all_use_dhtml_editor { get; set; }
        public bool use_rss_view { get; set; }
        public bool grp_use_rss_view { get; set; }
        public bool all_use_rss_view { get; set; }
        public bool use_good { get; set; }
        public bool grp_use_good { get; set; }
        public bool all_use_good { get; set; }
        public bool use_nogood { get; set; }
        public bool grp_use_nogood { get; set; }
        public bool all_use_nogood { get; set; }
        public bool use_name { get; set; }
        public bool grp_use_name { get; set; }
        public bool all_use_name { get; set; }
        public bool use_signature { get; set; }
        public bool grp_use_signature { get; set; }
        public bool all_use_signature { get; set; }
        public bool use_ip_view { get; set; }
        public bool grp_use_ip_view { get; set; }
        public bool all_use_ip_view { get; set; }
        public bool use_list_content { get; set; }
        public bool grp_use_list_content { get; set; }
        public bool all_use_list_content { get; set; }
        public bool use_list_file { get; set; }
        public bool grp_use_list_file { get; set; }
        public bool all_use_list_file { get; set; }
        public bool use_list_view { get; set; }
        public bool grp_use_list_view { get; set; }
        public bool all_use_list_view { get; set; }
        public bool use_email { get; set; }
        public bool grp_use_email { get; set; }
        public bool all_use_email { get; set; }
        public bool use_cert { get; set; }
        public bool grp_use_cert { get; set; }
        public bool all_use_cert { get; set; }
        public string upload_count { get; set; }
        public bool grp_upload_count { get; set; }
        public bool all_upload_count { get; set; }

        [MaxLength(10)]
        public string upload_size { get; set; }
        public bool grp_upload_size { get; set; }
        public bool all_upload_size { get; set; }
        public bool use_file_content { get; set; }
        public bool grp_use_file_content { get; set; }
        public bool all_use_file_content { get; set; }

        [MaxLength(4)]
        public string write_min { get; set; }
        public bool grp_write_min { get; set; }
        public bool all_write_min { get; set; }

        [MaxLength(4)]
        public string write_max { get; set; }
        public bool grp_write_max { get; set; }
        public bool all_write_max { get; set; }

        [MaxLength(4)]
        public string comment_min { get; set; }
        public bool grp_comment_min { get; set; }
        public bool all_comment_min { get; set; }

        [MaxLength(4)]
        public string comment_max { get; set; }
        public bool grp_comment_max { get; set; }
        public bool all_comment_max { get; set; }
        public bool use_sns { get; set; }
        public bool grp_use_sns { get; set; }
        public bool all_use_sns { get; set; }
        public bool use_search { get; set; }
        public bool grp_use_search { get; set; }
        public bool all_use_search { get; set; }

        [MaxLength(4)]
        public string bo_order { get; set; }
        public bool grp_order { get; set; }
        public bool all_order { get; set; }
        public bool use_captcha { get; set; }
        public bool grp_use_captcha { get; set; }
        public bool all_use_captcha { get; set; }
        public int skin_id { get; set; }
        public bool grp_skin { get; set; }
        public bool all_skin { get; set; }
        public int skin_mobile_id { get; set; }
        public bool grp_mobile_skin { get; set; }
        public bool all_mobile_skin { get; set; }

        [MaxLength(50)]
        public string include_head { get; set; }
        public bool grp_include_head { get; set; }
        public bool all_include_head { get; set; }
        public string include_tail { get; set; }
        public bool grp_include_tail { get; set; }
        public bool all_include_tail { get; set; }

        [AllowHtml]
        [MaxLength(500)]
        public string detail_hots { get; set; }
        public bool grp_detail_hots { get; set; }
        public bool all_detail_hots { get; set; }

        [AllowHtml]
        [MaxLength(500)]
        public string detail_tail { get; set; }
        public bool grp_detail_tail { get; set; }
        public bool all_detail_tail { get; set; }

        [AllowHtml]
        [MaxLength(500)]
        public string detail_mobile_hots { get; set; }
        public bool grp_detail_mobile_hots { get; set; }
        public bool all_detail_mobile_hots { get; set; }

        [AllowHtml]
        [MaxLength(500)]
        public string detail_mobile_tail { get; set; }
        public bool grp_detail_mobile_tail { get; set; }
        public bool all_detail_mobile_tail { get; set; }

        [MaxLength(500)]
        public string insert_content { get; set; }
        public bool grp_insert_content { get; set; }
        public bool all_insert_content { get; set; }

        [MaxLength(4)]
        public string subject_len { get; set; }
        public bool grp_subject_len { get; set; }
        public bool all_subject_len { get; set; }

        [MaxLength(4)]
        public string mobile_subject_len { get; set; }
        public bool grp_mobile_subject_len { get; set; }
        public bool all_mobile_subject_len { get; set; }

        [MaxLength(4)]
        public string page_rows { get; set; }
        public bool grp_page_rows { get; set; }
        public bool all_page_rows { get; set; }

        [MaxLength(4)]
        public string mobile_page_rows { get; set; }
        public bool grp_mobile_page_rows { get; set; }
        public bool all_mobile_page_rows { get; set; }

        [MaxLength(2)]
        public string gallery_cols { get; set; }
        public bool grp_gallery_cols { get; set; }
        public bool all_gallery_cols { get; set; }

        [MaxLength(4)]
        public string gallery_width { get; set; }
        public bool grp_gallery_width { get; set; }
        public bool all_gallery_width { get; set; }

        [MaxLength(4)]
        public string gallery_height { get; set; }
        public bool grp_gallery_height { get; set; }
        public bool all_gallery_height { get; set; }

        [MaxLength(4)]
        public string mobile_gallery_width { get; set; }
        public bool grp_mobile_gallery_width { get; set; }
        public bool all_mobile_gallery_width { get; set; }

        [MaxLength(4)]
        public string mobile_gallery_height { get; set; }
        public bool grp_mobile_gallery_height { get; set; }
        public bool all_mobile_gallery_height { get; set; }

        [MaxLength(4)]
        public string table_width { get; set; }
        public bool grp_table_width { get; set; }
        public bool all_table_width { get; set; }

        [MaxLength(4)]
        public string image_width { get; set; }
        public bool grp_image_width { get; set; }
        public bool all_image_width { get; set; }

        [MaxLength(4)]
        public string icon_new { get; set; }
        public bool grp_icon_new { get; set; }
        public bool all_icon_new { get; set; }

        [MaxLength(4)]
        public string icon_hot { get; set; }
        public bool grp_icon_hot { get; set; }
        public bool all_icon_hot { get; set; }

        [MaxLength(4)]
        public string reply_order { get; set; }
        public bool grp_reply_order { get; set; }
        public bool all_reply_order { get; set; }

        [MaxLength(11)]
        public int sort_field { get; set; }
        public bool grp_sort_field { get; set; }
        public bool all_sort_field { get; set; }
        public bool grp_point { get; set; }

        [MaxLength(5)]
        public string read_point { get; set; }
        public bool grp_read_point { get; set; }
        public bool all_read_point { get; set; }

        [MaxLength(5)]
        public string write_point { get; set; }
        public bool grp_write_point { get; set; }
        public bool all_write_point { get; set; }

        [MaxLength(5)]
        public string comment_point { get; set; }
        public bool grp_comment_point { get; set; }
        public bool all_comment_point { get; set; }

        [MaxLength(5)]
        public string download_point { get; set; }
        public bool grp_download_point { get; set; }
        public bool all_download_point { get; set; }

        [MaxLength(255)]
        public string subj1 { get; set; }

        [MaxLength(255)]
        public string txt1 { get; set; }
        public bool grp_1 { get; set; }
        public bool all_1 { get; set; }

        [MaxLength(255)]
        public string subj2 { get; set; }

        [MaxLength(255)]
        public string txt2 { get; set; }
        public bool grp_2 { get; set; }
        public bool all_2 { get; set; }

        [MaxLength(255)]
        public string subj3 { get; set; }

        [MaxLength(255)]
        public string txt3 { get; set; }
        public bool grp_3 { get; set; }
        public bool  all_3 { get; set; }

        [MaxLength(255)]
        public string subj4 { get; set; }

        [MaxLength(255)]
        public string txt4 { get; set; }
        public bool grp_4 { get; set; }
        public bool  all_4 { get; set; }

        [MaxLength(255)]
        public string subj5 { get; set; }

        [MaxLength(255)]
        public string txt5 { get; set; }
        public bool grp_5 { get; set; }
        public bool  all_5 { get; set; }

        [MaxLength(255)]
        public string subj6 { get; set; }

        [MaxLength(255)]
        public string txt6 { get; set; }
        public bool grp_6 { get; set; }
        public bool  all_6 { get; set; }

        [MaxLength(255)]
        public string subj7 { get; set; }

        [MaxLength(255)]
        public string txt7 { get; set; }
        public bool grp_7 { get; set; }
        public bool  all_7 { get; set; }

        [MaxLength(255)]
        public string subj8 { get; set; }

        [MaxLength(255)]
        public string txt8 { get; set; }
        public bool grp_8 { get; set; }
        public bool  all_8 { get; set; }

        [MaxLength(255)]
        public string subj9 { get; set; }

        [MaxLength(255)]
        public string txt9 { get; set; }
        public bool grp_9 { get; set; }
        public bool  all_9 { get; set; }

        [MaxLength(255)]
        public string subj10 { get; set; }

        [MaxLength(255)]
        public string txt10 { get; set; }
        public bool grp_10 { get; set; }
        public bool  all_10 { get; set; }


    }
}
