﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class WithdrawalListDto
    {
        public int id { get; set; }
        public string username { get; set; }
        public string fullname { get; set; }
        public string name { get; set; }
        public string amount { get; set; }
        public string charge { get; set; }
        public string total { get; set; }
        public string create_date { get; set; }
        public string start_date { get; set; }
        public string stop_date { get; set; }
        public string active { get; set; }

    }
}
