﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class DashboardDto
    {
        public List<tb1> tab1 { get; set; }
        public List<tb2> tab2 { get; set; }
        public List<tb3> tab3 { get; set; }

        public DashboardDto()
        {
            tab1 = new List<tb1>();
            tab2 = new List<tb2>();
            tab3 = new List<tb3>();
        }
    }

    public class tb1
    {
        public string username { get; set; }
        public string fullname { get; set; }
        public string nickname { get; set; }
        public string member_no { get; set; }
        public int point { get; set; }

    }
    public class tb2
    {
        public string grp_name { get; set; }
        public string app_name { get; set; }
        public string title { get; set; }
        public string create_by { get; set; }
        public string create_date { get; set; }

    }
    public class tb3
    {
        public string p_username { get; set; }
        public string p_name { get; set; }
        public string p_nickname { get; set; }
        public string p_create_date { get; set; }
        public string p_detail { get; set; }
        public string p_amount { get; set; }
        public string p_total { get; set; }

    }
}
