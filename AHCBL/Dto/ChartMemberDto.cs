﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto
{
    public class ChartMemberDto
    {
        public int id { get; set; }
        public string username { get; set; }
        public string fullname { get; set; }
        public string adviser { get; set; }
    }
}
