﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto
{
    public class LogDto
    {
        public int id { get; set; }
        public string ip { get; set; }
        public string url { get; set; }
        public string browser { get; set; }
        public string system_os { get; set; }
        public string connect_hardware { get; set; }
        public string start_date { get; set; }
        public string st_date { get; set; }
        public string stop_date { get; set; }
        public int member_id { get; set; }


    }
}
