﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.User
{
    public class OrderDto
    {
        public List<Order1> tab1 { get; set; }
        public List<Order2> tab2 { get; set; }
        public List<OrderHis> orderHis { get; set; }

        public OrderDto()
        {
            tab1 = new List<Order1>();
            tab2 = new List<Order2>();
            orderHis = new List<OrderHis>();
        }
        public class Order1
        {
            public int id { get; set; }
            public string token { get; set; }
            public string name { get; set; }
            public string img { get; set; }
            public int buy { get; set; }
            public int buy_appv { get; set; }
            public int appv { get; set; }
            public int complete { get; set; }
            public int sale { get; set; }
        }
        public class Order2
        {
            public int id { get; set; }
            public string hp { get; set; }
            public string token { get; set; }
            public string img { get; set; }
            public string name { get; set; }
            public string create_date { get; set; }
            public string price { get; set; }
            public string bank_name { get; set; }
            public string acc_no { get; set; }
            public string acc_name { get; set; }
            public string fullname { get; set; }
            public int status { get; set; }
            public string sale_date { get; set; }
            public string sale_by { get; set; }
            public int percen { get; set; }
            public string buy_info { get; set; }
            public string win_price { get; set; }
        }

        public class OrderHis
        {
            public string create_date { get; set; }
            public string status { get; set; }
            public string his_name { get; set; }
            public string amount { get; set; }
            
        }
    }
}
