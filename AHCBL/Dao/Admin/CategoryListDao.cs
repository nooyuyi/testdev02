﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class CategoryListDao : BaseDao<CategoryListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<CategoryListDto> GetDataList()
        {
            try
            {
                List<CategoryListDto> list = new List<CategoryListDto>();
                //dt = GetStoredProc("PD009_GET_CATEGORY");
                dt = GetStoredProc("PD009_GET_PRODUCT");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new CategoryListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        code = Util.NVLString(dr["code"]),
                        name = Util.NVLString(dr["name"]),
                        min_price = Util.NVLString(dr["min_price"]),
                        max_price = Util.NVLString(dr["max_price"]),
                        charging_time = Util.NVLString(dr["charging_time"]),
                        energy = Util.NVLInt(dr["energy"]),
                        revenue = Util.NVLInt(dr["revenue"]),
                        day = Util.NVLInt(dr["day"]),
                        win_count = Util.NVLInt(dr["win_count"]),
                        win_max_price = Util.NVLString(dr["win_max_price"]),
                        ca_order = Util.NVLString(dr["ca_order"]),
                        ca_use = Util.NVLBool(dr["ca_use"]),
                        product_amt = Util.NVLInt(dr["product_amt"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }


        public string SaveDataList(CategoryListDto model, string action)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD010_SAVE_CATEGORY", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@code", Util.NVLString(model.code));
                AddSQLParam(param, "@name", Util.NVLString(model.name));
                AddSQLParam(param, "@min_price", Util.NVLString(model.min_price));
                AddSQLParam(param, "@max_price", Util.NVLString(model.max_price));
                AddSQLParam(param, "@charging_time", Util.NVLString(model.charging_time));
                AddSQLParam(param, "@energy", Util.NVLInt(model.energy));
                AddSQLParam(param, "@revenue", Util.NVLInt(model.revenue));
                AddSQLParam(param, "@day", Util.NVLInt(model.day));
                AddSQLParam(param, "@win_count", Util.NVLInt(model.win_count));
                AddSQLParam(param, "@win_max_price", Util.NVLString(model.win_max_price));
                AddSQLParam(param, "@ca_order", Util.NVLInt(model.ca_order));               
                AddSQLParam(param, "@ca_use", Util.NVLBool(model.ca_use));
                AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));
                AddSQLParam(param, "@active", Util.NVLInt(model.active));
                AddSQLParam(param, "@status", action);

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}
