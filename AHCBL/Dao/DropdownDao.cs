﻿using AHCBL.Component.Common;
using AHCBL.Dto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AHCBL.Dao
{
    public class DropdownDao : BaseDao<DropdownDao>
    {
        private DataTable dt;
        #region DropDown Group
        [NonAction]
        public SelectList GetDataGroup()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD025_GET_GROUP", new string[] { "@fix" }, new string[] { Util.NVLString(0) });
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["id"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }
        public SelectList GetDataGroup(int id)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            //dt = GetStoredProc("PD025_GET_GROUP");
            dt = GetStoredProc("PD025_GET_GROUP", new string[] { "@fix" }, new string[] { Util.NVLString(id) });
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["id"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }
        #endregion

        #region DropDown Drive

        [NonAction]
        public SelectList GetDataDrive()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD026_GET_DRIVE");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["id"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }

        #endregion

        [NonAction]
        public SelectList GetDataAdmin()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD036_GET_ADMIN", new string[] { "@memberlevel" }, new string[] { Util.NVLString(1) });
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["id"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetDataCaptcha()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD037_GET_CAPTCHA");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["id"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetDataConfirm()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD044_GET_CONFIRM");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["id"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }
        
        [NonAction]
        public SelectList GetCatagoryList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD045_GET_CATEGORY");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["id"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }

        [NonAction]
        public SelectList GetDataBank()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD050_GET_BANK");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["id"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetDataPoint()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD058_GET_PRODUCT_QTY");
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["name"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetMemberList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "회원아이디", Value = "id" });
            list.Add(new SelectListItem { Text = "닉네임", Value = "nick" });
            list.Add(new SelectListItem { Text = "이름", Value = "name" });
            list.Add(new SelectListItem { Text = "권한", Value = "level" });
            list.Add(new SelectListItem { Text = "E-MAIL", Value = "email" });
            list.Add(new SelectListItem { Text = "전화번호", Value = "tel" });
            list.Add(new SelectListItem { Text = "휴대폰번호", Value = "hp" });
            list.Add(new SelectListItem { Text = "포인트", Value = "point" });
            list.Add(new SelectListItem { Text = "가입일시", Value = "createdate" });
            list.Add(new SelectListItem { Text = "IP", Value = "ip" });
            list.Add(new SelectListItem { Text = "추천인", Value = "adviser" });
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetDrpPoint(int i)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "회원아이디", Value = "username" });
            if (i != 1)
            {
                list.Add(new SelectListItem { Text = "내용", Value = "name" });
            }
            return new SelectList(list, "Value", "Text");
        }


        [NonAction]
        public SelectList GetStatusTran()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "대기", Value = "1" });
            list.Add(new SelectListItem { Text = "완료", Value = "3" });
            return new SelectList(list, "Value", "Text");
        }



        [NonAction]
        public SelectList GetStatus()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "대기", Value = "2" });
            list.Add(new SelectListItem { Text = "완료", Value = "3" });
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetAnswer()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "나중에 쓴 답변 아래로 달기 (기본)", Value = "1" });
            list.Add(new SelectListItem { Text = "나중에 쓴 답변 위로 달기", Value = "0" });
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetSortField()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "wr_num, wr_reply : 기본", Value = "1" });
            list.Add(new SelectListItem { Text = "wr_datetime asc : 날짜 이전것 부터", Value = "2" });
            list.Add(new SelectListItem { Text = "wr_datetime desc : 날짜 최근것 부터", Value = "3" });
            list.Add(new SelectListItem { Text = "wr_hit asc : 조회수 낮은것 부터", Value = "4" });
            list.Add(new SelectListItem { Text = "wr_hit desc : 조회수 높은것 부터", Value = "5" });
            list.Add(new SelectListItem { Text = "wr_last asc : 최근글 이전것 부터", Value = "6" });
            list.Add(new SelectListItem { Text = "wr_last desc : 최근글 최근것 부터", Value = "7" });
            list.Add(new SelectListItem { Text = "wr_comment asc : 댓글수 낮은것 부터", Value = "8" });
            list.Add(new SelectListItem { Text = "wr_comment desc : 댓글수 높은것 부터", Value = "9" });
            list.Add(new SelectListItem { Text = "wr_good asc : 추천수 낮은것 부터", Value = "10" });
            list.Add(new SelectListItem { Text = "wr_good desc : 추천수 높은것 부터", Value = "11" });
            list.Add(new SelectListItem { Text = "wr_nogood asc : 비추천수 낮은것 부터", Value = "12" });
            list.Add(new SelectListItem { Text = "wr_nogood desc : 비추천수 높은것 부터", Value = "13" });
            list.Add(new SelectListItem { Text = "wr_subject asc : 제목 오름차순", Value = "14" });
            list.Add(new SelectListItem { Text = "wr_subject desc : 제목 내림차순", Value = "15" });
            list.Add(new SelectListItem { Text = "wr_name asc : 글쓴이 오름차순", Value = "16" });
            list.Add(new SelectListItem { Text = "wr_name desc : 글쓴이 내림차순", Value = "17" });
            list.Add(new SelectListItem { Text = "ca_name asc : 분류명 오름차순", Value = "18" });
            list.Add(new SelectListItem { Text = "ca_name desc : 분류명 내림차순", Value = "19" });  
            return new SelectList(list, "Value", "Text");
        }
        
        [NonAction]
        public SelectList GetMemberNo()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "1", Value = "1" });
            list.Add(new SelectListItem { Text = "2", Value = "2" });
            list.Add(new SelectListItem { Text = "3", Value = "3" });
            list.Add(new SelectListItem { Text = "4", Value = "4" });
            list.Add(new SelectListItem { Text = "5", Value = "5" });
            list.Add(new SelectListItem { Text = "6", Value = "6" });
            list.Add(new SelectListItem { Text = "7", Value = "7" });
            list.Add(new SelectListItem { Text = "8", Value = "8" });
            list.Add(new SelectListItem { Text = "9", Value = "9" });
            list.Add(new SelectListItem { Text = "10", Value = "10" });
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetMemberLevel()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "1", Value = "1" });
            list.Add(new SelectListItem { Text = "2", Value = "2" });
            list.Add(new SelectListItem { Text = "3", Value = "3" });
            list.Add(new SelectListItem { Text = "4", Value = "4" });
            list.Add(new SelectListItem { Text = "5", Value = "5" });
            list.Add(new SelectListItem { Text = "6", Value = "6" });
            list.Add(new SelectListItem { Text = "7", Value = "7" });
            list.Add(new SelectListItem { Text = "8", Value = "8" });
            list.Add(new SelectListItem { Text = "9", Value = "9" });
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetMemberIcon()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "미사용", Value = "0" });
            list.Add(new SelectListItem { Text = "아이콘만 표시", Value = "1" });
            list.Add(new SelectListItem { Text = "아이콘+이름 표시", Value = "2" });
            return new SelectList(list, "Value", "Text");
        }

        [NonAction]
        public SelectList GetDataSkin(string str)
        {
            //pc,pc-mobile,mobile
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD043_GET_SKIN", new string[] { "@skin" }, new string[] { Util.NVLString(str) });
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new SelectListItem()
                {
                    Text = row["name"].ToString(),
                    Value = row["id"].ToString()
                });
            }
            return new SelectList(list, "Value", "Text");
        }

        [NonAction]
        public SelectList GetSkin()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "선택", Value = "0" });
            list.Add(new SelectListItem { Text = "(테마) basic", Value = "1"});
            list.Add(new SelectListItem { Text = "basic", Value = "2" });
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetLink()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "_blank", Value = "1" });
            list.Add(new SelectListItem { Text = "_self", Value = "2" });
            list.Add(new SelectListItem { Text = "_top", Value = "3" });
            list.Add(new SelectListItem { Text = "_new", Value = "4" });
            return new SelectList(list, "Value", "Text");
        }

        [NonAction]
        public SelectList GetIdCard()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "사용안함", Value = "0" });
            list.Add(new SelectListItem { Text = "테스트", Value = "1" });
            list.Add(new SelectListItem { Text = "실서비스", Value = "2" });
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetIPIN()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "사용안함", Value = "0" });
            list.Add(new SelectListItem { Text = "코리아크레딧뷰로(KCB) 아이핀", Value = "1" });
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetCertHP()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "사용안함", Value = "0" });
            list.Add(new SelectListItem { Text = "코리아크레딧뷰로(KCB) 휴대폰 본인확인", Value = "1" });
            list.Add(new SelectListItem { Text = "NHN KCP 휴대폰 본인확인", Value = "2" });
            list.Add(new SelectListItem { Text = "LG유플러스 휴대폰 본인확인", Value = "2" });
            return new SelectList(list, "Value", "Text");
        }
        
        [NonAction]
        public SelectList GetSMS()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "사용안함", Value = "0" });
            list.Add(new SelectListItem { Text = "아이코드", Value = "1" });
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetSMSType()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "SMS", Value = "1" });
            list.Add(new SelectListItem { Text = "LMS", Value = "2" });
            return new SelectList(list, "Value", "Text");
        }
        [NonAction]
        public SelectList GetNoTi()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "사용안함", Value = "0" });
            list.Add(new SelectListItem { Text = "사용함", Value = "1" });
            return new SelectList(list, "Value", "Text");
        }

        [NonAction]
        public SelectList GetUsecret()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "사용하지 않음", Value = "0" });
            list.Add(new SelectListItem { Text = "체크박스", Value = "1" });
            list.Add(new SelectListItem { Text = "무조건", Value = "2" });
            return new SelectList(list, "Value", "Text");
        }
        
    }
}
