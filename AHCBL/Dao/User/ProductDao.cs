﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.User
{
    public class ProductDao : BaseDao<ProductDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<ItemListDto> GetDataList()
        {
            try
            {
                List<ItemListDto> list = new List<ItemListDto>();
                dt = GetStoredProc("PD048_GET_PRODUCT");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new ItemListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                        name = Util.NVLString(dr["name"]),
                        img = Util.NVLString(dr["img"]),
                        price = Util.NVLString(Util.NVLInt(dr["price"]) < 1000 ? dr["price"].ToString() : Util.NVLInt(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        price_win = Util.NVLString(Util.NVLInt(dr["price_win"]) < 1000 ? dr["price_win"].ToString() : Util.NVLInt(dr["price_win"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        percen = Util.NVLInt(dr["percen"]),
                        day = Util.NVLInt(dr["day"]),
                        chip = Util.NVLInt(dr["chip"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<ItemListDto> GetProductOrder(int fag)
        {
            try
            {
                List<ItemListDto> list = new List<ItemListDto>();
                dt = GetStoredProc("PD057_GET_PRODUCT_ORDER", new string[] { "@fag","@member_id" }, new string[] { Util.NVLString(fag), Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new ItemListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        name = Util.NVLString(dr["name"]),
                        img = Util.NVLString(dr["img"]),
                        //amount = Util.NVLInt(dr["amount"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public List<ItemListDto> GetOrderList()
        {
            try
            {
                List<ItemListDto> list = new List<ItemListDto>();
                dt = GetStoredProc("PD059_GET_ORDER_LIST", new string[] { "@member_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new ItemListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                        chip = Util.NVLInt(dr["add_chip"]),
                        amount = Util.NVLInt(dr["buy_chip"]),
                        total = Util.NVLInt(dr["total"]),
                        status = Util.NVLString(dr["name"]),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public string SaveDataList(ItemListDto model)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                //MySqlCommand cmd = new MySqlCommand("PD049_BUY_SALE_PRODUCT", conn);
                MySqlCommand cmd = new MySqlCommand("PD049_BUY_PRODUCT", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@productid", Util.NVLInt(model.id));
                AddSQLParam(param, "@p_amount", Util.NVLInt(model.amount));
                AddSQLParam(param, "@memberid", Util.NVLInt(Varible.User.member_id));
                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        //public string SaveDataList(ItemListDto model, string fag)
        //{
        //    string result = "OK";
        //    try
        //    {
        //        conn = CreateConnection();
        //        MySqlCommand cmd = new MySqlCommand("PD049_BUY_SALE_PRODUCT", conn);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        MySqlParameterCollection param = cmd.Parameters;
        //        param.Clear();
        //        AddSQLParam(param, "@product_id", Util.NVLInt(model.id));
        //        AddSQLParam(param, "@chip", Util.NVLString(model.chip));
        //        AddSQLParam(param, "@price", Util.NVLString(model.price));
        //        AddSQLParam(param, "@point", Util.NVLString(model.point));
        //        AddSQLParam(param, "@percen", Util.NVLInt(model.percen));
        //        AddSQLParam(param, "@amount", Util.NVLInt(model.amount));
        //        AddSQLParam(param, "@total", Util.NVLString(model.total));
        //        AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));
        //        AddSQLParam(param, "@fag", Util.NVLString(fag));
        //        conn.Open();
        //        MySqlDataReader read = cmd.ExecuteReader();
        //        while (read.Read())
        //        {
        //            result = read.GetString(0).ToString();
        //        }
        //        conn.Close();
        //    }
        //    catch (Exception e)
        //    {
        //        result = e.Message.ToString();
        //    }
        //    return result;
        //}
    }
}