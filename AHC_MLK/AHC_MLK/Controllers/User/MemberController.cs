﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dao.User;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.User
{
    public class MemberController : Controller
    {
        Permission checkuser = new Permission();
        // GET: Member
        public ActionResult Index()
        {
            if (ModelState.IsValid)
            {
                checkuser.chkrights("user");
                //ViewBag.Bank = DropdownDao.Instance.GetDataBank();
                ViewBag.OldPass = Varible.User.pass;
                var model = MemberListDao.Instance.GetDataList().Find(smodel => smodel.id == Varible.User.member_id);
                var bank_id = DropdownDao.Instance.GetDataBank();
                model.bank_list = new SelectList(bank_id, "Value", "Text");


                return View(model);
            } 
            else
            {
                return View();
            }
                
        }
        public ActionResult ChangeBank(int id,int bank_id,string acc_no,string acc_name)
        {
            try
            {

                MemberListDto model = new MemberListDto();
                model.id = id;
                model.bank_id = bank_id;
                model.acc_no = acc_no;
                model.acc_name = acc_name;

                string result = MemberDao.Instance.ChangeBank(model);
                if (result != "OK")
                {
                    //ViewBag.Message = result;
                    return Json(new returnsave { err = "1", errmsg = result });
                }
                else
                {
                    //return RedirectToAction("Member");
                    return Json(new returnsave { err = "0", errmsg = "Successfully !" });
                }

            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error" });
            }


            //return objreturnsave;
        }
        //[HttpPost]
        //public ActionResult ChangeBank(MemberListDto model)
        //{
        //    try
        //    {

        //        string result = MemberDao.Instance.ChangeBank(model);
        //        if (result != "OK")
        //        {
        //            ViewBag.Message = result;
        //        }
        //        else
        //        {
        //            //return RedirectToAction("Member");
        //            return View("Member");
        //        }
        //        return View();


        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}



        public ActionResult ChangePass(int id ,string pass)
        {
            try
            {
                MemberDao.Instance.ChangePass(id,pass);
                return Json("Successfully !");
            }
            catch (Exception e)
            {
                return Json("Error");
            }
        }


        //[HttpPost]
        //public ActionResult ChangePass(MemberListDto model)
        //{
        //    try
        //    {
        //        if (Varible.User.pass != model.password)
        //        {
        //            return Json(new { status = "Error", message = "Password incorrect !" });
        //        }
        //        //string result = MemberDao.Instance.ChangeBank(model);
        //        //if (result != "OK")
        //        //{
        //        //    ViewBag.Message = result;
        //        //}
        //        //else
        //        //{
        //        //    return RedirectToAction("Index");
        //        //}
        //        return View();
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

    }
}