﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dao.User;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.User
{
    public class OrderController : Controller
    {
        Permission checkuser = new Permission();

        // GET: Order
        //public ActionResult Index(int? id)
        //{
        //    checkuser.chkrights("user");
        //    //Request["id"].ToString();
        //    int status = 1;
        //    status = id != null ? Util.NVLInt(id) : 1;
        //    ViewBag.amount = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);// Varible.User.amount;
        //    //var data = ProductDao.Instance.GetProductOrder(1);
        //    //var data = OrderDao.Instance.GetProductOrder(1);
        //    int rows = Util.NVLInt(ConfigurationManager.AppSettings["rows"]);
        //    ViewBag.rows = rows;

        //    var data = OrderDao.Instance.GetProductOrder(status);
        //    //return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(rows)));
        //    //var data = OrderDao.Instance.GetProductOrder(1).Find(smodel => smodel.id == id);
        //    return View(data);
        //}

        //[HttpGet]
        public ActionResult Index(string token, string status, int? page)
        {
            checkuser.chkrights("user");
            ViewBag.amount = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);// Varible.User.amount;
                                                                                        //var data = ProductDao.Instance.GetProductOrder(1);
                                                                                        //var data = OrderDao.Instance.GetProductOrder(1);
            int rows = Util.NVLInt(ConfigurationManager.AppSettings["rows"]);
            ViewBag.rows = rows;

            int id = 0;
            if (token != null)
            {
                id = Util.NVLInt(DataCryptography.Decrypt(token));
            }
            var data = OrderDao.Instance.GetProductOrder(id, status);
            return View(data);

            //return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(rows)));
            //var data = OrderDao.Instance.GetProductOrder(1).Find(smodel => smodel.id == id);
            //    return View(data);
            //return View(data);
        }
        public ActionResult Sell_list()
        {
            checkuser.chkrights("user");
            ViewBag.amount = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);// Varible.User.amount;
            //var data = ProductDao.Instance.GetProductOrder(2);
            var data = OrderDao.Instance.GetProductOrder(0,"");
            return View(data);
        }
        public ActionResult Order_have()
        {
            checkuser.chkrights("user");
            var data = OrderDao.Instance.GetProductOrder(0,"");
            return View(data);
        }
        public ActionResult Order_buy_point()
        {
            checkuser.chkrights("user");
            

            //var bank = DropdownDao.Instance.GetDataBank().Find(smodel => smodel.id == Varible.User.member_id);
            ViewBag.product_amt = Varible.Config.jewel_price.Replace(".00","");// ConfigurationManager.AppSettings["product_amt"];
            ViewBag.exchange = Varible.Config.exchange.Replace(".00", ""); //Util.NVLDecimal(Varible.Config.exchange).ToString("0,0", CultureInfo.InvariantCulture);
            ViewBag.bank_info = Varible.Config.bank_info;// ConfigurationManager.AppSettings["bank_info"];
            ViewBag.amount = GetDataDao.Instance.GetPointAmount(Varible.User.member_id); //Varible.User.amount;
            ViewBag.product_qty = DropdownDao.Instance.GetDataPoint();
            return View();
        }
        
        public ActionResult BuyChip(string buy_name, int point, int amount, int price, int pay_type)
        {
            try
            {

                PointListDto model = new PointListDto();
                model.username = buy_name;
                model.name = "이체";
                model.pay_type = pay_type;
                model.amount = Util.NVLString(amount);
                model.price = Util.NVLString(price);
                model.total = Util.NVLString((point+ amount));
                string result = PointListDao.Instance.SaveDataList(model, "mony");
                if (result != "OK")
                {
                    return Json(result);
                }
                else
                {
                    return Json("Successfully !");
                }

            }
            catch (Exception e)
            {
                return Json("Error !!");
            }
        }
        public ActionResult Order_send_point()
        {
            checkuser.chkrights("user");
            ViewBag.amount = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);
            return View();
        }
        public ActionResult TranPoint(int point,string pass )
        {
            try
            {
                if (pass != Varible.User.pass)
                {
                    return Json(new returnsave { err = "1", errmsg = "비밀번호가 일치하지 않습니다." });
                }
                else
                {
                    int p_id = Util.NVLInt(Session["member_id"]);
                    string result = PointListDao.Instance.TranPoint(0,point, p_id,"");
                    if (result != "OK")
                    {
                        return Json(new returnsave { err = "1", errmsg = "Error" });
                    }
                    else
                    {
                        return Json(new returnsave { err = "0", errmsg = "꿀벌 전송이 완료되었습니다." });
                    }
                }
            }catch(Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }
        }
        public ActionResult ChkUserID(string pUid)
        {
            try
            {

                int member_id = 0;
                string pUName = string.Empty;
                int member_level = 0;
                string password = string.Empty;
                string amount = string.Empty;
                Session.Remove("member_id");
                bool result = LoginDao.Instance.ChkAuthDB(pUid, ref member_id, ref pUName, ref member_level, ref password, ref amount);
                if (result == false)
                {
                    return Json(new returnsave { err = "1", errmsg = "회원을 찾을수 없습니다." });
                }
                else
                {
                    Session["member_id"] = member_id;
                    return Json(new returnsave { err = "0", errmsg = "아이디가 확인되었습니다.\n꿀벌 전송을 진행해주세요." });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "0", errmsg = "Error" });
            }
           

            //return objreturnsave;
        }
        public ActionResult Order_list_point(int? page)
        {
            checkuser.chkrights("user");
            int rows = Util.NVLInt(ConfigurationManager.AppSettings["rows"]);
            var data = ProductDao.Instance.GetOrderList();
            return View(data.ToList().ToPagedList(page ?? 1, rows));
        }
        public ActionResult Sell_apply(string Token)
        {
            checkuser.chkrights("user");
            try
            {
                int id = Util.NVLInt(DataCryptography.Decrypt(Token));
                var data = OrderDao.Instance.GetProductAppv(id);
                if (data.tab2.Count != 0)
                {
                    return View(data);
                }
                else
                {
                    ViewBag.amount = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);// Varible.User.amount;
                                                                                                //var data = ProductDao.Instance.GetProductOrder(2);
                    data = OrderDao.Instance.GetProductOrder(0, "");
                    return View("~/Views/Order/Sell_list.cshtml", data);
                    //return View(data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult Order_apply(string Token)
        {
            checkuser.chkrights("user");
            try
            {
                int id = Util.NVLInt(DataCryptography.Decrypt(Token));
                var data = OrderDao.Instance.GetProductSale(id);
                return View(data);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult Appv(string buy_info,string token)
        {
            try
            {
                int id = Util.NVLInt(DataCryptography.Decrypt(token));
                string result = OrderDao.Instance.SaveData(id, buy_info, buy_info == "" ? "apv" : "buy");
                if (result != "OK")
                {
                    return Json(new returnsave { err = "1", errmsg = "Error" });
                }
                else
                {
                    return Json(new returnsave { err = "0", errmsg = buy_info=="" ? "판매확정이 완료되었습니다.": "구매확정이 완료되었습니다." });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }
        }
        public ActionResult Cancel(string token)
        {
            try
            {
                int id = Util.NVLInt(DataCryptography.Decrypt(token));
                string result = OrderDao.Instance.SaveData(id, "", "del");
                if (result != "OK")
                {
                    return Json(new returnsave { err = "1", errmsg = "Error" });
                }
                else
                {
                    return Json(new returnsave { err = "0", errmsg = "구매 신청이 취소되었습니다." });
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }
        }
        public ActionResult Order_withdraw_point()
        {
            try
            {
                checkuser.chkrights("user");
                var model = MemberListDao.Instance.GetDataList().Find(smodel => smodel.id == Varible.User.member_id);
                decimal point = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);
                ViewBag.amount = point.ToString("0,0", CultureInfo.InvariantCulture) + " Point  (￦ "+ (point * Util.NVLDecimal(Varible.Config.jewel_price)).ToString("0,0", CultureInfo.InvariantCulture) + ")";


                //ViewBag.amount = point + "Point (￦ "+ Convert.ToDecimal(((point * Util.NVLInt(Varible.Config.jewel_price)).ToString()).ToString()).ToString("0,0", CultureInfo.InvariantCulture) +")";

                return View(model);
            }
            catch(Exception e)
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult WithdrawPoint(int amt, int point, string pass)
        {
            try
            {
                if (pass != Varible.User.pass)
                {
                    return Json(new returnsave { err = "1", errmsg = "비밀번호가 일치하지 않습니다." });
                }
                else
                {
                    //int p_id = Util.NVLInt(Varible.User.member_id);
                    string result = PointListDao.Instance.TranPoint(amt, point, Varible.User.member_id, "tran");
                    if (result != "OK")
                    {
                        return Json(new returnsave { err = "1", errmsg = "Error" });
                    }
                    else
                    {
                        return Json(new returnsave { err = "0", errmsg = "출금신청이 정상적으로 처리 되었습니다." });
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new returnsave { err = "1", errmsg = "Error" });
            }
        }
    }
}