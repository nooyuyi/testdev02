﻿using AHCBL.Component.Common;
using AHCBL.Dao.Admin;
using AHCBL.Dao.User;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.User
{
    public class NoticeController : Controller
    {
         Permission checkuser = new Permission();
        // GET: Notice
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("user");
            var data = ContentListDao.Instance.GetDataList();
            return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"])));
        }
        //public ActionResult Notice_detail(int? page)
        //{
        //    //var data = ContentListDao.Instance.GetDataList();
        //    //return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"])));
        //    return View();
        //}
        public ActionResult Notice_detail(int? id)
        {
            checkuser.chkrights("user");
            var data = ContentListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            return View(data);
        }
        public ActionResult qna(int? page)
        {
            checkuser.chkrights("user");
            var data = NoticeDao.Instance.GetDataQA();
            return View(data.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"])));
        }
        public ActionResult qna_detail(int id)
        {
            checkuser.chkrights("user");
            var data = NoticeDao.Instance.GetDataQA().Find(smodel => smodel.id == id);
            ViewBag.comment = data.comment;
            return View(data);
        }
        
        public ActionResult qna_write()
        {
            checkuser.chkrights("user");
            return View();
        }
        [HttpPost]
        public ActionResult qna_write(string content)
        {
            try
            {
                string result= NoticeDao.Instance.SaveData(content);
                if (result!="OK")
                {
                    return Json("Error");
                }
                else
                {
                    return Json("1:1 문의가 정상적으로 등록 되었습니다.");
                }
            }
            catch (Exception e)
            {
                return Json("Error");
            }
        }

    }
}