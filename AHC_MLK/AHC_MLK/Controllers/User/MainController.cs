﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dao.User;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.User
{
    public class MainController : Controller
    {
        Permission checkuser = new Permission();
        // GET: main
        public ActionResult Index()
        {
            checkuser.chkrights("user");
            ViewBag.amount = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);
            var pm = GetDataDao.Instance.GetProductMember();
            ViewBag.PM = pm;


            int rows = Util.NVLInt(ConfigurationManager.AppSettings["rows"]);
            var data = OrderDao.Instance.GetOrderHis();
            ViewBag.his_tb = data;
            ViewBag.cnt = data.Count();

            //var dataNotice = NoticeDao.Instance.GetDataQA();
            //ViewBag.dataNotice = dataNotice;
            //ViewBag.cntNotice = dataNotice.Count();
            int? page = null;
            var dataNotice = ContentListDao.Instance.GetDataList();
            ViewBag.dataNotice = dataNotice.ToList().ToPagedList(page ?? 1, Util.NVLInt(ConfigurationManager.AppSettings["rows"]));
            ViewBag.cntNotice = dataNotice.Count();

            return View();
        }
    }
}