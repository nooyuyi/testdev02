﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class WithdrawalListController : Controller
    {
        Permission checkuser = new Permission();
        // GET: Withdrawal
        public ActionResult Index(string drp, string keyword, int? page)
        {
            checkuser.chkrights("admin");
            var data = WithdrawalListDao.Instance.GetDataList();
            var amount = GetDataDao.Instance.GetWithdrawalSumAmount("");
            var dropdown = DropdownDao.Instance.GetDrpPoint(0);
            int rows = Util.NVLInt(ConfigurationManager.AppSettings["rows"]);

            string count = string.Empty;
            string total = string.Empty;


            if (amount < 1000)
            {
                total = amount.ToString();
            }
            else
            {
                total = amount.ToString("0,0", CultureInfo.InvariantCulture);
            }
            if (data.Count < 1000)
            {
                count = data.Count.ToString();
            }
            else
            {
                count = data.Count.ToString("0,0", CultureInfo.InvariantCulture);
            }

            ViewBag.Amount = total;
            ViewBag.Count = count;
            ViewBag.Search = dropdown;
            ViewBag.Rows = rows;

            if (drp == "username")
            {
                return View(data.Where(x => x.username == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, rows));
            }
            if (drp == "name")
            {
                return View(data.Where(x => x.name == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, rows));
            }

            return View(data.ToList().ToPagedList(page ?? 1, rows));

            //return View(WithdrawalListDao.Instance.GetDataList());
        }
    }
}