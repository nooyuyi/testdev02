﻿
using AHCBL.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace AHC_MLK.Controllers.Admin
{
    public class ChartMemberController : Controller
    {
        // GET: ChartMember
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetChartData()
        {
            try
            {
                //var data = ChartMemberDao.Instance.GetDataList();
                var data = ChartMemberDao.Instance.GetChartData();
                // return Json data;
                return Json(new { data = data });
            }
            catch (Exception e)
            {
                return null;
            }
        }
        //[WebMethod]
        //public static List<object> GetChartData()
        //{
        //    //string query = "SELECT MemberId, Name, ParentId";
        //    //query += " FROM FamilyHierarchy";
        //    //string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
        //    //using (SqlConnection con = new SqlConnection(constr))
        //    //{
        //    //    using (SqlCommand cmd = new SqlCommand(query))
        //    //    {
        //    //        List<object> chartData = new List<object>();
        //    //        cmd.CommandType = CommandType.Text;
        //    //        cmd.Connection = con;
        //    //        con.Open();
        //    //        using (SqlDataReader sdr = cmd.ExecuteReader())
        //    //        {
        //    //            while (sdr.Read())
        //    //            {
        //    //                chartData.Add(new object[]
        //    //                {
        //    //                sdr["MemberId"], sdr["Name"], sdr["ParentId"]
        //    //                });
        //    //            }
        //    //        }
        //    //        con.Close();
        //    //        return chartData;
        //    //    }
        //    //}
        //    return null;
        //}

    }
}