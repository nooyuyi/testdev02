﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AHC_MLK.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(LoginDto model)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    bool flgLogin = true;
                    int member_id = 0;
                    string pUName = string.Empty;
                    int member_level = 0;
                    string password = string.Empty;
                    string amount = string.Empty;
                    string pUid = Util.NVLString(model.username);
                    string pAss = Util.NVLString(model.password);
                    if (pUid == "" || pUid == null)
                    {
                        //ViewBag.Message = "Username or Password Incorrect";
                        return View();
                    }
                    if (pAss == "" || pAss == null)
                    {
                        //ViewBag.Message = "Username or Password Incorrect";
                        return View();
                    }
                    //LoginDao.Instance.ChkAuthDB(pUid, DataCryptography.Encrypt(pPwd), ref member_id, ref pUName, ref pCenter, ref pChannel);

                    if (LoginDao.Instance.ChkAuthDB(pUid, ref member_id, ref pUName, ref member_level, ref password, ref amount))
                    {
                        EUser user = new EUser();
                        user.UserIP = GetUserIP();
                        user.member_id = member_id;
                        user.UserID = pUid;
                        user.UserName = pUName;
                        user.Login = DateTime.Now;

                        Varible.User = user;
                        Varible.User.member_id = member_id;
                        Varible.User.lelvel = member_level;
                        Varible.User.amount = amount;
                        Varible.User.pass = pAss;
                        //เช็ค password ถูกหรือไม่
                        string pass = DataCryptography.Decrypt(password.ToString().Trim());
                        if (model.password != pass)
                        {
                            ViewBag.Message = "Username or Password Incorrect";
                            //UserDao.Instance.SaveUserLog("Log in", "Log in", "ขออภัย User ของท่านระบุ Password ไม่ถูกต้อง");
                            //return;
                            return View();
                        }
                        Session["username"] = Varible.User.UserName;
                        var da = Request.Browser.IsMobileDevice;
                        LogDto log = new LogDto();
                        log.ip = user.UserIP;
                        log.url = Request.Url.AbsoluteUri;
                        log.browser = GetWebBrowserName();
                        log.system_os = Info.GetHardware();
                        log.connect_hardware = Request.Browser.IsMobileDevice==true ? "Mobile" : "Computer";
                        //log.member_id = Util.NVLInt(Varible.User.member_id);

                        //Session["Register"] = Request.Url.Authority + "/Register/?Token="+ DataCryptography.Encrypt(pUName);
                        Session["Register"] = Request.Url.Authority + "/Register/?code=" + pUName;
                        LoginDao.Instance.LogLogin(log,"login");
                        if (member_level == 2)
                        {
                            Response.Redirect("~/Main", false);
                        }
                        else if (member_level == 1)
                        {
                            Response.Redirect("~/Dashboard", false);
                        }

                    }
                    else
                    {
                        ViewBag.Message = "Username or Password Incorrect";
                    }
                    return View();
                } 
                else
                {
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.Message = "Error : " + e.Message;
                return View();
            }
        }


        private string GetUserIP()
        {
            string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return Request.ServerVariables["REMOTE_ADDR"];
        }
        public string GetWebBrowserName()
        {
            string WebBrowserName = string.Empty;
            try
            {
                WebBrowserName = Request.Browser.Browser;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return WebBrowserName;
        }
        public ActionResult LogOut()
        {
            LogDto log = new LogDto();
            LoginDao.Instance.LogLogin(log, "logout");
            FormsAuthentication.SignOut();
            Session.Abandon(); // it will clear the session at the end of request
            return RedirectToAction("Index", "Login");
        }
    }
}